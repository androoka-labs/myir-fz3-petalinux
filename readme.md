
# MYIR-FZ3 Petalinux

### Description
This is a basic petalinux project. It is intended to utilize the <a href=https://gitlab.com/androoka-labs/myir-fz3.git>myir-fz3</a> development board. It starts with running a vivado tcl script 
> src/create_project.tcl  
this creates 
1. the basic project
2. adds any vhd/vhdl files from the src directory, setting _tb.vhd[l] files to option synthesis false. **Note that if the directory name they reside in is not "vhdl", then the vhdl files will also be marked as residing in a package equal to the directory name.** To bypass this library addition,add a second directory "vhdl" to the module.
3. adds any xdc files from the src directory.
4. runs src/myzynq.tcl which creates a block diagram with Zynq A53/R5 cores and configures the peripherals according to the MYIR-FZ3 schematic diagram.
5. create wrapper for myzynq and sets it as the top level.
6. compiles the project and exports the xsa.


to create the petalinux project from scratch.
> petalinux-create -t project -n santiago --template zynqMP  
> petalinux-config --get-hw-description ../fpga/myzynq.xsa  

and modify the primary SD/SDIO card.  

![SDCARD](docs/sdcard_selection.png)

also, change a few image packaging options. in this case, the filesystem is an ext4 partition. this is optional, but allows users to keep filesystems changes between reboots.
![ImageOptions](docs/image_packaging.png)

another required change is to change the device-tree files so that Linux doesn't get confused over voltages of the sd card. this is required as vivado is configure to run 1.8v for the sd card, however the sd card correctly reports that it runs at 3.3v. This is because there a voltage translator between 1.8v and 3.3v and there is no setting in vivado that overrides this situation. if this is not set, then the sd card is rejected and the system is unable to boot from /dev/mmcblk1p2 and mmcblk1 will not be available.

> nano project-spec/meta-user/recipes-bsp/device-tree/files/system-user.dtsi  

`/include/ "system-conf.dtsi"`  
`/ {  `  
`};  `  
`   `  
**`&sdhci1 {`**  
**`  no-1-8-v;`**  
**`};`**  
`   `  

> petalinux-build  

| after compilation, images/linux/rootfs.ext4 is copied to the sdcard using  
> sudo dd if=images/linux/rootfs.ext4 of=/dev/sd?2 bs=4k  
> sudo resize2fs /dev/sd?2  

noting the sd?2 needs to be mmc sd card device. please be careful with this as you can corrupt working filesystems if you get the wrong disk. the alternative is to <a href="https://unix.stackexchange.com/questions/141255/give-a-specific-user-permissions-to-a-device-without-giving-access-to-other-user">change permissions</a> of the usb/sd card reader 

